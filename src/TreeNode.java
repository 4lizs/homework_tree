import java.util.*;

public class TreeNode {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	TreeNode(String n, TreeNode d, TreeNode r) {
		name = n;
		firstChild = d;
		nextSibling = r;
	}

	public static TreeNode parsePrefix(String s) {
		if (s == null || s.isEmpty()) {
			return null;
		}

		StringTokenizer mingi = new StringTokenizer(s, ",()", true);
		String midagi = mingi.nextToken();
		String Q = "", E = "", V = "";

		while (mingi.hasMoreTokens()) {
			Q = mingi.nextToken();
			if (Q.equals("(")) {
				Q = mingi.nextToken();
				int closed = 0;
				while (closed < 1) {
					E = E + Q;
					Q = mingi.nextToken();
					closed = findBrackets(Q, closed);
				}
			}
			if (Q.equals(",")) {
				while (mingi.hasMoreTokens()) {
					V = V + mingi.nextToken();
				}
			}
		}
		return new TreeNode(midagi, TreeNode.parsePrefix(E), TreeNode.parsePrefix(V));
	}

	private static int findBrackets(String str, int num) {
		if (str.equals("(")) {
			return num - 1;
		} else if (str.equals(")")) {
			return num + 1;
		}
		return num;
	}

	public String rightParentheticRepresentation() {
		String Returned = "";
		if (firstChild != null) {
			Returned += "(" + firstChild.rightParentheticRepresentation() + ")";
		}
		Returned += name;
		if (nextSibling != null) {
			Returned += "," + nextSibling.rightParentheticRepresentation();
		}
		return Returned;
	}
	

	
	public static void main(String[] param) {
		String s = "A(B,C(R(T)))";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B,C) ==> (B,C)A

}
}