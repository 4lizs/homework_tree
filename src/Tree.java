
import java.util.*;

public class Tree implements java.util.Iterator<Tree> {

   private String name;
   private Tree rightSibling;
   private Tree firstChild;
   private int info;

   Tree (String s, Tree p, Tree a) {
      setName (s);
      setRightSibling (p);
      setFirstChild (a);
   }

   Tree (String s, Tree p, Tree a, int i) {
      setName (s);
      setRightSibling (p);
      setFirstChild (a);
      setInfo (i);
   }

   Tree() {
      this ("", null, null, 0);
   }

   Tree (String s) {
      this (s, null, null, 0);
   }

   Tree (String s, Tree p) {
      this (s, p, null, 0);
   }

   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setRightSibling (Tree p) {
      rightSibling = p;
   }

   public Tree getRightSibling() {
      return rightSibling;
   }

   public void setFirstChild (Tree a) {
      firstChild = a;
   }

   public Tree getFirstChild() {
      return firstChild;
   }

   public void setInfo (int i) {
      info = i;
   }

   public int getInfo() {
      return info;
   }

   @Override
   public String toString() {
      return leftParentheticRepresentation();
   }

   public void processTreeNode() {
      System.out.print (getName() + "  ");
   }

   public boolean hasNext() {
      return (getRightSibling() != null);
   }

   public Tree next() {
      return getRightSibling();
   }

   public void remove() {
      throw new UnsupportedOperationException();
   }

   public Iterator<Tree> children() {
      return getFirstChild();
   }

   public void addChild (Tree a) {
      if (a == null) return;
      Iterator<Tree> children = children();
      if (children == null)
         setFirstChild (a);
      else {
         while (children.hasNext())
            children = (Tree)children.next();
         ((Tree)children).setRightSibling (a);
      }
   }

   public boolean isLeaf() {
      return (getFirstChild() == null);
   }

   public int size() {
      int n = 1; // root
      Iterator<Tree> children = children();
      while (children != null) {
         n = n + ((Tree)children).size();
         children = (Tree)children.next();
      }
      return n;
   }

   public void preorder() {
      processTreeNode();
      Iterator<Tree> children = children();
      while (children != null) {
         ((Tree)children).preorder();
         children = (Tree)children.next();
      }
   }

   public void postorder() {
      Iterator<Tree> children = children();
      while (children != null) {
         ((Tree)children).postorder();
         children = (Tree)children.next();
      }
      processTreeNode();
   }

   public String leftParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      b.append (getName());
      // TODO!!!
      return b.toString();
   }

   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      // TODO!!!
      return b.toString();
   }
 
   public static Tree createTree() {
      Tree root = new Tree ("+", null, 
         new Tree ("*",
            new Tree ("/", null,
               new Tree ("6",
                  new Tree ("3", null, null),
                  null)),
            new Tree ("-",
               new Tree ("4", null, null),
               new Tree ("2",
                  new Tree ("1", null, null),
                  null))));     
      return root;
   }

   public static void main (String[] param) {
      Tree t = createTree();
      System.out.println ("Number of nodes: " + String.valueOf (t.size()));
      System.out.print ("Preorder: ");
      t.preorder();
      System.out.println();
      System.out.print ("Postorder: ");
      t.postorder();
      System.out.println();
   } // main

} // TreeNode


